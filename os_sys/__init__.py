
import os
import sys


__all__ = ['os_sys', 'fail', 'modules', 'system', 'wifi', 'programs', 'test', 'code', 'decode', 'discription', '_code', 'more_input', 'all_dict', 'download',
           'obj_type', 'object_type', 'show_progress', 'update_progress', 'progress_bar_loading', 'tqdm', 'progress_types', 'bar', 'tqdm_gui', 'gui_bar']
__fail__ = ['warn_return', 'make_warn', 'print_warn', 'warn_msg', 'warning_msg', 'warn_file_no', 'msg', 'module_warn', 'text_warn']
__os_sys__ = ['main_dir', 'get_import_list', 'get_user', 'cmd', 'info', 'win_version', 'cmd_filter_haak', 'filter_regel', 'cmd_out_list',
           'cmd_out', 'ColorPrint', 'info', 'is_connected', 'ping', 'connect_time', 'internet',
           'chek_speed', 'internet_and_speed', 'cmd_ping', 'cmd',
           'ping_data', 'replace', 'open_site', 'explorer_dict', 'explorer',
           'is_even', 'is_oneven', 'fahr_to_celsius', 'celsius_to_kelvin', 'fahr_to_kelvin', 'convert_c_to_f'
           ]
__all_names__ = __os_sys__
fail_ = __fail__
__all = []
os_sys_ = __all_names__
index = 0
os_all = []
while index < len(os_sys_):
    __all.append(''.join('os_sys.' + os_sys_[index]))
    index += 1
index = 0

index = 0
fail_all = []
while index < len(fail_):
    __all.append(''.join('fail.' + fail_[index]))
    index += 1

def _code(txt):
    b = txt
    d = {}
    for c in (65, 97):
        for i in range(26):
            d[chr(i+c)] = chr((i+13) % 26 + c)

    data = "".join([d.get(c, c) for c in b])
    
    return data
import requests
def download(url, file, path=None):
    url = url  
    r = requests.get(url)
    
    print('downloading:')
    import os
    filepath = os.path.join(path, file) if not file and path == None else os.path.join(os.path.abspath(''), file)
    with open(str(filepath), 'wb') as f:  
        f.write(r.content)
def os_sys_lib():
    from distutils.sysconfig import get_python_lib as gpl
    path = os.path.join(str(gpl()), 'os_sys')
    return path
def more_input():
    init = input()
    mystr = str()
    while not init == 'None':
        mystr = mystr + (str(init)) + '\n'
        init = input()
    
    return mystr

def print_all_dirs(start_dir, except_dir):
    for dirname, dirnames, filenames in os.walk(start_dir):
        # print path to all subdirectories first.
        for subdirname in dirnames:
            print(os.path.join(dirname, subdirname))

        # print path to all filenames.
        for filename in filenames:
            print(os.path.join(dirname, filename))

        # Advanced usage:
        # editing the 'dirnames' list will stop os.walk() from recursing into there.
        if except_dir in dirnames:
            # don't go into any .git directories.
            dirnames.remove(except_dir)
class cmd_parser:
    import argparse
    exeple = '''
    def main():
        ''\' Example of taking inputs for megazord bin''\'
        parser = argparse.ArgumentParser(prog='my_megazord_program')
        parser.add_argument('-i', nargs='?', help='help for -i blah')
        parser.add_argument('-d', nargs='?', help='help for -d blah')
        parser.add_argument('-v', nargs='?', help='help for -v blah')
        parser.add_argument('-w', nargs='?', help='help for -w blah')

        args = parser.parse_args()

        collected_inputs = {'i': args.i,
                        'd': args.d,
                        'v': args.v,
                        'w': args.w}
        print 'got input: ', collected_inputs
import argparse

def main():
    \''' Example of taking inputs for megazord bin''\'
    parser = argparse.ArgumentParser(prog='my_megazord_program')
    parser.add_argument('-i', nargs='?', help='help for -i blah')
    parser.add_argument('-d', nargs='?', help='help for -d blah')
    parser.add_argument('-v', nargs='?', help='help for -v blah')
    parser.add_argument('-w', nargs='?', help='help for -w blah')

    args = parser.parse_args()

    collected_inputs = {'i': args.i,
                    'd': args.d,
                    'v': args.v,
                    'w': args.w}
    print 'got input: ', collected_inputs



    '''
    
def make_text(file):
    try:
        fh = open(str(file) + '.lib', mode='r', encoding='utf-8')
    except Exception:
        data = ''
        pass
    else:
        data = _code(fh.read())
        fh.close()
        print(data)
    fh = open(str(file) + '.lib', mode='w', encoding='utf-8')
    fh.write(str(_code(str(data + more_input()))))
    fh.close()
def object_type(obj):
    m = type(obj)
    t = m
    m = str(t).replace('<class \'', '')
    t = m
    m = str(t).replace('\'>', '')
    return m
obj_type = object_type
from tqdm import tqdm_gui as gui_bar
from tqdm import tqdm_gui

def all_dict(dictory, exceptions=None, file_types=None, maps=True, files=False, print_data=False):
    import os
    data = []
    string_data = ''
    e = exceptions
    if 'list' in str(type(e)) or e == None:
        pass
    else:
        raise TypeError('expected a list but got a %s' % type(e))
    e = file_types
    if 'list' in str(type(e)) or e == None:
        pass
    else:
        raise TypeError('expected a list but got a %s' % type(e))
    
    print_ = True if print_data == 'print' or print or True else False
    
    for dirname, dirnames, filenames in os.walk(dictory):
        # print path to all subdirectories first.
        if maps:
            for subdirname in dirnames:
                dat = os.path.join(dirname, subdirname)
                data.append(dat)
                string_data = string_data + '\n' + dat
                if print_:
                    print(dat)

        # print path to all filenames.
        if files:
            for filename in filenames:
                s = False
                fname_path = filename
                file = fname.split('.')
                no = int(len(file) - 1)
                file_type = file[no]
                if not e == None:
                    for b in range(0, len(e)):
                        if e[b] == file_type:
                            s = True
                            
                if e == None:
                    s = True
                if s:
                    s = False   
                            
                    dat = os.path.join(dirname, filename)
                    data.append(dat)
                    string_data = string_data + '\n' + dat
                    if print_:
                        
                        print(dat)
        

        # Advanced usage:
        # editing the 'dirnames' list will stop os.walk() from recursing into there.
        if not exceptions == None:
            
            for ip in range(0, int(len(exceptions))):
                exception = exceptions[ip]
                
                if exception in dirnames:
                    # don't go into any .git directories.
                    dirnames.remove(exception)
    
    return [data, string_data]
import os
import time
def show_progres():
    import time, sys
    from tqdm import tqdm
    for i in tqdm(range(10)):
         time.sleep(1)
import time, sys

# update_progress() : Displays or updates a console progress bar
## Accepts a float between 0 and 1. Any int will be converted to a float.
## A value under 0 represents a 'halt'.
## A value at 1 or bigger represents 100%
def update_progress(progress):
    barLength = 10 # Modify this to change the length of the progress bar
    status = ""
    if isinstance(progress, int):
        progress = float(progress)
    if not isinstance(progress, float):
        progress = 0
        status = "error: progress var must be float\r\n"
    if progress < 0:
        progress = 0
        status = "Halt...\r\n"
    if progress >= 1:
        progress = 1
        status = "Done...\r\n"
    block = int(round(barLength*progress))
    text = "\rPercent: [{0}] {1}% {2}".format( "#"*block + "-"*(barLength-block), progress*100, status)
    sys.stdout.write(text + '\n')
    sys.stdout.flush()
def test():
    # update_progress test script
    print("progress : 'hello'")
    update_progress("hello")
    

    print("progress : 3")
    update_progress(3)
    

    print("progress : [23]")
    update_progress([23])
    
    print("")
    print("progress : -10")
    update_progress(-10)
    

    print("")
    print("progress : 10")
    update_progress(10)
   

    print("")
    print("progress : 0->1")
    for i in range(100):
        
        update_progress(i/100.0)

    print("")
    print("Test completed")
import sys
import time
import threading
stop = False
kill = False
class progress_bar_loading(threading.Thread):
    __all__ = ['run', 'kill']
    def run(self):
            global stop
            global kill
            print('Loading....  ')
            sys.stdout.flush()
            i = 0
            while stop != True:
                    if (i%4) == 0: 
                        sys.stdout.write('\b')
                    elif (i%4) == 1: 
                        sys.stdout.write('\b')
                    elif (i%4) == 2: 
                        sys.stdout.write('\b')
                    elif (i%4) == 3: 
                        sys.stdout.write('\b')

                    sys.stdout.flush()
                    time.sleep(0.2)
                    i+=1

            if kill:
                print('\b\b\b\b ABORT!')
            else: 
                print('\b\b done!')
    def kill(self):
        global kill
        global stop
        kill = True
        stop = True

kill = False
import threading
from threading import Thread
run_background = threading.Thread
run_apart = threading.Thread
from tqdm import tqdm as _t_q_d_m_
class tqdm_loop(Thread):
    global kill
    '''
tqdm help
  """
  Decorate an iterable object, returning an iterator which acts exactly
  like the original iterable, but prints a dynamically updating
  progressbar every time a value is requested.
  """

  def __init__(self, iterable=None, desc=None, total=None, leave=True,
               file=None, ncols=None, mininterval=0.1,
               maxinterval=10.0, miniters=None, ascii=None, disable=False,
               unit='it', unit_scale=False, dynamic_ncols=False,
               smoothing=0.3, bar_format=None, initial=0, position=None,
               postfix=None, unit_divisor=1000):
               '''
    __all__ = ['run']
    def __init__(self, _range, sleep):
        self.u = _range
        self.sleep_time = sleep
    def run(self):
        loop = True
        if loop:
            for i in _t_q_d_m_(self.u):
                from time import sleep
                sleep(self.sleep_time)
                if kill:
                    break
            return
    
        
class tqdm(Thread):
    
    '''
tqdm help
  """
  Decorate an iterable object, returning an iterator which acts exactly
  like the original iterable, but prints a dynamically updating
  progressbar every time a value is requested.
  """

  def __init__(self, iterable=None, desc=None, total=None, leave=True,
               file=None, ncols=None, mininterval=0.1,
               maxinterval=10.0, miniters=None, ascii=None, disable=False,
               unit='it', unit_scale=False, dynamic_ncols=False,
               smoothing=0.3, bar_format=None, initial=0, position=None,
               postfix=None, unit_divisor=1000 total=100):
               '''
    __all__ = ['run']
    def __init__(self, args):
        self.args = args
        self.args[total] = 100 if total not in args else args[total]
        self.sleep_time = args[sleep] if sleep in args else 0.1
        bar = tqdm(self.args)
    def update(self, n=1):
        bar.update(n)
    def run(self, between):
        for i in tqdm(range(self.args[total]), self.args):
            import time
            time.sleep(between)
    def close():
        bar.close()
bar = progress_bar_loading()



if __name__ == '__main__':
    test()
try:
    from . import fail, modules, system, wifi, programs, test, os_sys, errors, discription, progress_bars, _progress as progress
except ImportError:
    pass
    try:
        from os_sys import fail, modules, system, wifi, programs, test, os_sys, errors, discription, progress_bars, _progress as progress
    except Exception:
        pass
        import fail, modules, system, wifi, programs, test, os_sys, errors, discription, progress_bars, _progress as progress
        
fail = fail
modules = modules
system = system
wifi = wifi
programs = programs
test = test
os_sys = os_sys
errors = errors
discription = discription
decode = discription
code = discription
progress_bar = progress_bars
progres_bar = progress_bars
progress = progress
progres = progress
def bar(rn, fill='.'):
    import time



    loading = '\b' * rn  # for strings, * is the repeat operator
    rest = fill * int(100 - rn)

    # this loop replaces each dot with a hash!
    print('[\r%0s%1s] loading at %2d percent!' % (loading, rest, rn), end='\n')

if __name__ == '__main__':
     for rn in range(1, 101):
        bar(rn)
from progress.bar import *
from progress.spinner import *
from progress.counter import *
class progress_types:
    __all__ = ['bar', 'charging_bar', 'filling_sqares_bar', 'filling_circles_bar', 'incremental_bar', 'pixel_bar',
               'shady_bar', 'spinner', 'pie_spinner', 'moon_spinner', 'line_spinner', 'pixel_spinner',
               'counter', 'countdown', 'stack', 'pie']
    bar = Bar
    charging_bar = ChargingBar
    filling_sqares_bar = FillingSquaresBar
    filling_circles_bar = FillingCirclesBar
    incremental_bar = IncrementalBar
    pixel_bar = PixelBar
    shady_bar = ShadyBar
    spinner = Spinner
    pie_spinner = PieSpinner
    moon_spinner = MoonSpinner
    line_spinner = LineSpinner
    pixel_spinner = PixelSpinner
    counter = Counter
    countdown = Countdown
    stack = Stack
    pie = Pie

progres_types = progress_types
progress_types = progress_types
if __name__ == '__main__':
    bar = Bar('Processing', max=20)
    for i in range(20):
        # Do some work
        bar.next()
        print('')
    bar.finish()
